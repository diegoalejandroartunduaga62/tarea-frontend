import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ListaTarea from '../views/TaskView.vue'
import Tarea from '../views/TaskNewView.vue'
import Login from '../views/LoginView.vue'
import Logout from '../views/LogoutView.vue'
import Error from '../views/ErrorView.vue'
import Register from '../views/RegisterView.vue'
import TareaOk from '../views/TaskViewOk.vue'
import User from '../views/UserView.vue'

const routes = [
  {
    path: '/listaTarea',
    name: 'listaTarea',
    component: ListaTarea,
    meta: { requiresAuth: true }
  },
  {
    path: '/tarea',
    name: 'tarea',
    component: Tarea,
    meta: { requiresAuth: true }
  },
  {
    path: '/tareaok',
    name: 'tareaok',
    component: TareaOk,
    meta: { requiresAuth: true }
  },
  {
    path: '/tareaedit',
    name: 'tareaedit',
    component: ()=> import('@/views/TaskViewEdit.vue')
  },
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout,
    meta: { requiresAuth: true }
  },
  {
    path: '/usuarios',
    name: 'usuarios',
    component: User,
    meta: { requiresAuth: true }
  },
  {
    path: '/error',
    name: 'error',
    component: Error
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const token = localStorage.getItem('token');
    if (!token) {
      next({ name: 'login' });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
